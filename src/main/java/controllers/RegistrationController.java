package controllers;

import domain.SignUpData;
import org.glassfish.jersey.media.multipart.FormDataParam;
import services.RegistrationService;
import services.interfaces.IRegistrationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/registration")
public class RegistrationController {
    private IRegistrationService registrationService = new RegistrationService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signUp(@FormDataParam("name") String name,
                           @FormDataParam("surname" ) String surname,
                           @FormDataParam("username") String username,
                           @FormDataParam("password") String password){
        SignUpData data = new SignUpData(name,surname,username,password);
        try {
            registrationService.addNewUser(data);
            return Response
                    .status(Response.Status.OK)
                    .entity("Well come new User!")
                    .build();
        } catch (ServerErrorException exception) {
            return Response
                    .status(Response.Status.METHOD_NOT_ALLOWED)
                    .entity(exception.getMessage())
                    .build();
        }
    }
}

package controllers;

import domain.AccessToken;
import domain.SignInData;
import services.AuthorizationService;
import services.interfaces.IAuthorizationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/authorization")
public class AuthorizationController {

    private final IAuthorizationService authorizationService = new AuthorizationService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signIn(SignInData data){
        try {
           AccessToken token = authorizationService.authenticateUser(data);
           return Response.ok(token).build();
        }
        catch (Exception exception){
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity(exception.getMessage())
                    .build();
        }

    }

}

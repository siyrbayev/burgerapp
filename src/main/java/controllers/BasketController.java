package controllers;

import domain.models.BasketItem;
import filters.customAnnotations.JWTTokenNeeded;
import services.BasketService;
import services.interfaces.IBasketService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@JWTTokenNeeded
@Path("/basket")
public class BasketController {

    private IBasketService basketService = new BasketService();

    @GET
    @JWTTokenNeeded
    @Path("/get")
    public Response getByUsername(@Context ContainerRequestContext requestContext){
        String username = requestContext.getSecurityContext().getUserPrincipal().getName();
        List<BasketItem> basketItems;
        try {
            basketItems = basketService.getBasket(username);
        } catch (ServerErrorException exception) {
            return Response
                    .serverError().build();
        } catch (BadRequestException exception) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(exception.getMessage()).build();
        }
        if (basketItems == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Basket does not exist!")
                    .build();
        }
        else {
            return Response
                    .status(Response.Status.OK)
                    .entity(basketItems)
                    .build();
        }
    }

    @JWTTokenNeeded
    @POST
    @Path("/buy/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response processOrder(@PathParam("id") long basketItemId, @Context ContainerRequestContext requestContext){
        String username = requestContext.getSecurityContext().getUserPrincipal().getName();
        try {
            basketService.buyByBasketItemId(basketItemId,username);
        } catch (ServerErrorException exception) {
            return Response
                    .serverError().build();
        } catch (BadRequestException exception) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(exception.getMessage()).build();
        }

        return Response
                .status(Response.Status.OK)
                .build();
    }
}

package controllers;

import domain.models.Product;
import domain.models.User;
import filters.customAnnotations.Admin;
import filters.customAnnotations.JWTTokenNeeded;
import org.glassfish.jersey.media.multipart.FormDataParam;
import services.UserService;
import services.interfaces.IUserService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.util.List;

@Path("/users")
public class UserController {

    private IUserService userService = new UserService();

    @GET
    public String hello() {
        return "Hello BOOOOooooooooooooooooooooooooooooooooooooy";
    }

    @JWTTokenNeeded
    @GET
    @Path("/{id}")
    public Response getUserByID(@PathParam("id") long id) {
        User user;
        try {
            user = userService.getUserByID(id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This user cannot be obtained").build();
        }
        if (user == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("User does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(user)
                    .build();
        }
    }
    @Admin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response createNewUser(User user) {
        try {
            userService.addUser(user);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }
        return Response.status(Response.Status.CREATED)
                .entity("User created successfully!")
                .build();
    }
    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updateUser(@FormDataParam("name") String name,
                               @FormDataParam("surname" ) String surname,
                               @FormDataParam("password") String password,
                               @FormDataParam("birthday") Date birthday,
                               @FormDataParam("phonenumber") String phonenumber,
                               @Context ContainerRequestContext requestContext) {
        String username = requestContext.getSecurityContext().getUserPrincipal().getName();
        User user = new User(name,surname,username,password,birthday,phonenumber);

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(user.getUsername())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            userService.updateUser(user);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.OK)
                .entity("User updated successfully!")
                .build();
    }



    @JWTTokenNeeded
    @GET
    @Path("/menu")
    public Response getMenu() {
        List<Product> products;
        try {
            products = userService.getMenu();
        } catch (ServerErrorException exception) {
            return Response
                    .serverError().build();
        } catch (BadRequestException exception) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(exception.getMessage()).build();
        }
        if (products == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Menu does not exist!")
                    .build();
        }
        else {
            return Response
                    .status(Response.Status.OK)
                    .entity(products)
                    .build();
        }

    }

    @GET
    @Path("menu/{id}/get?ingredients")
    public Response getIngridientsById(@PathParam("id") int id){

        return null;
    }
    @POST
    @JWTTokenNeeded
    @Path("/addtobasket/{id}/{quantity}")
    public Response addToBasket(@PathParam("id") long productId, @PathParam("quantity") int quantity, @Context ContainerRequestContext requestContext){

        String username = requestContext.getSecurityContext().getUserPrincipal().getName();
        try {
            userService.addToBasket(productId,username,quantity);

        }catch (ServerErrorException exception) {
            return Response
                    .serverError().build();
        } catch (BadRequestException exception) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(exception.getMessage()).build();
        }
        return Response
                .status(Response.Status.OK)
                .entity("Successfully added to basket")
                .build();
    }



}

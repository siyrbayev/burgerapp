import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
class BurgerApplication extends ResourceConfig {
    public BurgerApplication() {
        packages("controllers", "filters");
        packages("packages.to.scan");
        register(MultiPartFeature.class);
    }
}

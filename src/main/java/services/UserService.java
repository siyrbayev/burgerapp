package services;

import domain.models.Product;
import domain.models.User;
import repositories.entities.MenuRepository;
import repositories.entities.UserRepository;
import repositories.interfaces.IMenuRepository;
import repositories.interfaces.IUserRepository;
import services.interfaces.IUserService;

import java.util.List;

public class UserService implements IUserService {
    private IUserRepository userRepo =  new UserRepository();
    private IMenuRepository menuRepo = new MenuRepository();

    @Override
    public User getUserByID(long id) {
        return userRepo.getUserByID(id);
    }

    public User getUserByUsername(String username) {
        return userRepo.getUserByUsername(username);
    }

    public void addUser(User user) {
        userRepo.add(user);
    }

    public void updateUser(User user){
        userRepo.update(user);
    }

    public List<Product> getMenu() {
        return menuRepo.getMenu();
    }

    @Override
    public void addToBasket(Long product_id, String username, int quantity) {
        userRepo.addToBasket(product_id,username,quantity);
    }

}

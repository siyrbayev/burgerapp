package services;

import domain.SignUpData;
import repositories.entities.UserRepository;
import repositories.interfaces.IUserRepository;
import services.interfaces.IRegistrationService;

public class RegistrationService implements IRegistrationService {
    IUserRepository userRepository = new UserRepository();


    @Override
    public void addNewUser(SignUpData data) {
        userRepository.addNewUser(data);
    }
}

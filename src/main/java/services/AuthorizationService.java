package services;

import domain.AccessToken;
import domain.SignInData;
import domain.models.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import repositories.entities.UserRepository;
import repositories.interfaces.IUserRepository;
import services.interfaces.IAuthorizationService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Random;

public class AuthorizationService implements IAuthorizationService {
    private final IUserRepository userRepository = new UserRepository();

    public AccessToken authenticateUser(SignInData data) throws Exception {
        User authenticatedUser = signIn(data);
        return new AccessToken(issueToken(authenticatedUser));
    }

    private User signIn(SignInData data) throws Exception {
        User user = userRepository.findUserByLogin(data);
        if (user == null){
            throw new Exception("Authentication failed!");
        }
        return user;
    }
    public User getUserByUsername(String username){
        return userRepository.getUserByUsername(username);
    }

    private String issueToken(User user) { // ??????
        Instant now = Instant.now();
        String secretWord = "TheStrongestSecretKeyICanThinkOf1111111212121212121212";
        return Jwts.builder()
                .setIssuer(user.getUsername())
                .setIssuedAt(Date.from(now))
                .claim("1d20", new Random().nextInt(20) + 1)
                .setExpiration(Date.from(now.plus(20, ChronoUnit.MINUTES)))
                .signWith(Keys.hmacShaKeyFor(secretWord.getBytes()))
                .compact();
    }


}

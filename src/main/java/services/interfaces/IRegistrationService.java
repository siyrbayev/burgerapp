package services.interfaces;

import domain.SignUpData;

public interface IRegistrationService {

    void addNewUser(SignUpData data);
}

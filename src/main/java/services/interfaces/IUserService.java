package services.interfaces;
import domain.models.Product;
import domain.models.User;

import java.util.List;

public interface IUserService {

    User getUserByID(long id);

    User getUserByUsername(String username);

    void addUser(User user);

    void updateUser(User user);

    List<Product> getMenu();

    void addToBasket(Long product_id, String username,int quantity);
}

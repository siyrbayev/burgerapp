package services.interfaces;


import domain.AccessToken;
import domain.SignInData;
import domain.models.User;

public interface IAuthorizationService {
    AccessToken authenticateUser(SignInData data) throws Exception;
    User getUserByUsername(String issuer);
}


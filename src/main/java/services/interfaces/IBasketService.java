package services.interfaces;

import domain.models.BasketItem;

import java.util.List;

public interface IBasketService {

    void buyByBasketItemId(long basketItemId, String username);

    List<BasketItem> getBasket(String username);
}

package services;

import domain.models.BasketItem;
import repositories.entities.BasketRepository;
import repositories.interfaces.IBasketRepository;
import services.interfaces.IBasketService;

import java.util.List;

public class BasketService implements IBasketService {

    IBasketRepository basketRepo = new BasketRepository();


    @Override
    public void buyByBasketItemId(long basketItemId,String username){
        basketRepo.buyByBasketItemId(basketItemId,username);
    }
    @Override
    public List<BasketItem> getBasket(String username) {
        return basketRepo.getBasketItemsByUsername(username);
    }


}

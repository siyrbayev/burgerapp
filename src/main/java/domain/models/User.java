package domain.models;

import java.sql.Date;

public class User {
    private long id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private java.sql.Date birthday;
    private double balance; // in tg
    private String phoneNumber;
    private String role;




    public User(long id, String name, String surname, String username, String password, java.sql.Date birthday, double balance, String phoneNumber,String role) {
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setBirthday(birthday);
        setPhoneNumber(phoneNumber);
        setBalance(balance);
        setRole(role);
    }

    public User( String name, String surname, String username,String password, Date birthday, String phonenumber) {
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setBirthday(birthday);
        setPhoneNumber(phonenumber);
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String  phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public java.sql.Date getBirthday() {
        return birthday;
    }

    public void setBirthday(java.sql.Date birthday) {
        this.birthday = birthday;
    }
}

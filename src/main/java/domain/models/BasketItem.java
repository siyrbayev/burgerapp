package domain.models;

public class BasketItem {
    private long id;
    private long product_id;
    private int quantity;

    public BasketItem(Long id, long product_id, int quantity) {
        setId(id);
        setProduct_id(product_id);
        setQuantity(quantity);
    }

    public long getBasket_id() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

package domain.models;

import java.util.List;

public class Basket {
    private List<BasketItem> basketItems;
    private int user_id;

    public Basket(List<BasketItem> basketItems, int user_id) {
        this.basketItems = basketItems;
        this.user_id = user_id;
    }

    public List<BasketItem> getBasketItems() {
        return basketItems;
    }

    public void setBasketItems(List<BasketItem> basketItems) {
        this.basketItems = basketItems;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}

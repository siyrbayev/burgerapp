package repositories.entities;

import domain.models.Product;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IMenuRepository;

import javax.ws.rs.BadRequestException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MenuRepository implements IMenuRepository {

    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public List<Product> getMenu() {
        List<Product> products = new ArrayList<>();
        String sql = "Select category_name,name,price,description from menu ";
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Product product = new Product(
                        rs.getString("category_name"),
                        rs.getString("name"),
                        rs.getDouble("price"),
                        rs.getString("description")
                        );
                products.add(product);
            }
            return products;
        } catch (SQLException exception) {
            throw new BadRequestException("Can not run SQL statement" + exception.getMessage());
        }
    }
}

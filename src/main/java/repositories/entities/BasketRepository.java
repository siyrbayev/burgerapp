package repositories.entities;

import domain.RealDate;
import domain.models.BasketItem;
import repositories.db.PostgresRepository;
import repositories.interfaces.IBasketRepository;
import repositories.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BasketRepository implements IBasketRepository{

    IDBRepository dbrepo = new PostgresRepository();

    public void buyBasketItem(){

    }


    public List<BasketItem> getBasketItemsByUsername(String username) {
        String sql = "SELECT id,product_id, quantity " +
                " FROM public.basket_item where basket_id = (select id from basket where user_id = (select id from users where username = '"+ username +"'));";
        List<BasketItem> basketItems = new ArrayList<>();

        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BasketItem basketItem = new BasketItem(
                        rs.getLong("id"),
                        rs.getLong("product_id"),
                        rs.getInt("quantity")
                );
                basketItems.add(basketItem);
            }
            return basketItems;
        } catch (SQLException exception) {
            throw new BadRequestException("Can not run SQL statement" + exception.getMessage());
        }
    }

    public double getPriceByProductId(long product_id){
        Double price = null;
        String sql = "select price from menu where id = " + product_id + "limit 1";
        try{
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()){
                price = rs.getDouble("price");
            }

        }catch (SQLException exception){
            throw new BadRequestException("Can not run SQL statement: "+exception.getMessage());
        }
        return price;
    }
    public void addProductToOrderItem(long product_id, int quantity,Long order_id){

        double price = getPriceByProductId(product_id);
        RealDate realDate = new RealDate();
        Date data = realDate.realdate();
        String sql = "insert into order_item (order_id, product_id, price , quantity , date) " +
                "Values (?,?,?,?,?)";

        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,order_id);
            stmt.setLong(2,product_id);
            stmt.setDouble(3,price);
            stmt.setInt(4,quantity);
            stmt.setDate(5, data);
            stmt.execute();
        }catch (SQLException exception){
            throw new BadRequestException("Can not run SQL statement: "+exception.getMessage());
        }
    }
    public long getUserIdByUsername(String username){
        Long user_id = null;
        String sql = "select id from users where username = '" + username + "' limit 1;";
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                user_id = rs.getLong("id");
            }
        }catch (SQLException exception){
            throw new BadRequestException("Can not run SQL statement: "+exception.getMessage());
        }
        return user_id;

    }
    public long getOrderIdByUserName(String username){
        Long order_id = null;
        String sql = "select id from Public.order where user_id =(select id from users where username = '" + username + "') limit 1;";
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                order_id = rs.getLong("id");
            }
        }catch (SQLException exception){
            throw new BadRequestException("Can not run SQL statement: "+exception.getMessage());
        }
        return order_id;
    }
    @Override
    public void buyByBasketItemId(long basketItemId, String username) {
        long product_id = 0;
        int quantity = 0;
        String sql= "select product_id,quantity from basket_item where id = " + basketItemId + "limit 1";
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                product_id = rs.getLong("product_id");
                quantity = rs.getInt("quantity");
            }
            if (product_id == 0 && quantity == 0){
                throw new BadRequestException("NOT FIND ELEMENT");
            }

        }catch (SQLException exception){
            throw new BadRequestException("Can not run SQL statement: "+exception.getMessage());
        }

        addProductToOrderItem(product_id,quantity, getOrderIdByUserName(username));
    }

}

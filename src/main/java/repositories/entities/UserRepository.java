package repositories.entities;

import domain.SignInData;
import domain.SignUpData;
import domain.models.User;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IUserRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class UserRepository implements IUserRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    public void addNewUser(SignUpData data){
        try {
            String sql = "INSERT INTO users (name, surname, username, password) VALUES ( ?,?,?,?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, data.getName());
            stmt.setString(2, data.getSurname());
            stmt.setString(3, data.getUsername());
            stmt.setString(4, data.getPassword());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        createBasketAndOrderForUser(data.getUsername());
    }
    public Long getUserIdByUsername(String username) {
        Long user_id = null;
        try {
            String sql = "SELECT id FROM users WHERE username = ? limit 1";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {

                user_id = rs.getLong("id");

            }

        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return user_id;
    }
    public void createBasketAndOrderForUser(String username) {
        long user_id = getUserIdByUsername(username);
        String sqlOrder = "INSERT INTO public.order (id, user_id) " +
                "VALUES (?, ?);";

        String sqlBasket = "INSERT INTO public.basket (id, user_id) VALUES (?, ?);";
        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sqlOrder);
            stmt.setLong(1, user_id);
            stmt.setLong(2, user_id);
            stmt.execute();

            PreparedStatement stmt2 = dbrepo.getConnection().prepareStatement(sqlOrder);
            stmt2.setLong(1, user_id);
            stmt2.setLong(2, user_id);
            stmt2.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }



    @Override
    public void add(User entity) {
        try {
            String sql = "INSERT INTO users (name, surname, username, password, birthday, phonenumber) VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getSurname());
            stmt.setString(3, entity.getUsername());
            stmt.setString(4, entity.getPassword());
            stmt.setDate(5, entity.getBirthday());
            stmt.setString(6, entity.getPhoneNumber());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        createBasketAndOrderForUser(entity.getUsername());
    }
    public Long getBasketIdByUsername(String username){
        String sql = "Select id from basket where user_id = (select id from users where username = '"+ username + "' ) limit 1";
        Long basketId = null;
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()){
                basketId = rs.getLong("id");
            }
        }catch (SQLException exception) {
            exception.printStackTrace();
        }
        return basketId;
    }

    @Override
    public void addToBasket(long product_id,String username,int quantity){
        Long basketId = getBasketIdByUsername(username);

        String sql = "INSERT INTO basket_item (" +
                " basket_id, product_id, quantity)" +
                " VALUES ( ?, ?, ?) ;";
        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1,basketId);
            stmt.setLong(2,product_id);
            stmt.setInt(3,quantity);
            stmt.execute();
        } catch (SQLException exception) {
            throw new BadRequestException();
        }
    }
    @Override
    public void update(User user) {
        String sql = "UPDATE users " +
                "SET ";
        int c = 0;
        if (user.getName() != null) {
            sql += "name=?, "; c++;
        }
        if (user.getSurname() != null) {
            sql += "surname=?, "; c++;
        }
        if (user.getPassword() != null) {
            sql += "password=?, "; c++;
        }
        if (user.getBirthday() != null) {
            sql += "birthday=?, "; c++;
        }
        if (user.getPhoneNumber() != null) {
            sql += "phonenumber=?, "; c++;
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE username = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (user.getName() != null) {
                stmt.setString(i++, user.getName());
            }
            if (user.getSurname() != null) {
                stmt.setString(i++, user.getSurname());
            }
            if (user.getPassword() != null) {
                stmt.setString(i++, user.getPassword());
            }
            if (user.getBirthday() != null) {
                stmt.setDate(i++, user.getBirthday());
            }
            stmt.setString(i++, user.getUsername());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

    }

    @Override
    public void remove(User entity) {

    }

    @Override
    public Iterable<User> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<User> users = new LinkedList<>();
            while (rs.next()) {
                User user = new User(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday"),
                        rs.getFloat("balance"),
                        rs.getString("phonenumber"),
                        rs.getString("role")
                );

                users.add(user);
            }
            return users;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public User queryOne(String sql){
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()){
                return new User(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday"),
                        rs.getDouble("balance"),
                        rs.getString("phonenumber"),
                        rs.getString("role")
                );
            }
        }catch (SQLException exception){
            throw new BadRequestException("Can not run SQL statement: "+exception.getMessage());
        }
        return null;
    }

    @Override
    public User getUserByID(long id){
        String sql = "Select id,name,surname,username,password,birthday,balance,phonenumber,orders,role from users where id = " + id + " LIMIT 1";
        return queryOne(sql);
    }

    public User findUserByLogin(SignInData data) {
        String sql = "select * from users where username = ? And password = ? limit 1";
        try {
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1,data.getUsername());
            stmt.setString(2,data.getPassword());
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                User user = new User(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday"),
                        rs.getFloat("balance"),
                        rs.getString("phonenumber"),
                        rs.getString("role")
                );
                return user;
            }
        } catch (SQLException throwable) {
            throw new BadRequestException();
        }

        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        try {
            String sql = "SELECT * FROM users WHERE username = ? limit 1";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new User(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday"),
                        rs.getFloat("balance"),
                        rs.getString("phonenumber"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

}


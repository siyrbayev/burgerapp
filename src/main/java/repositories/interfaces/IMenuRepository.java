package repositories.interfaces;

import domain.models.Product;

import java.util.List;

public interface IMenuRepository {
    List<Product> getMenu();
}

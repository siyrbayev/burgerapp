package repositories.interfaces;

import domain.SignInData;
import domain.SignUpData;
import domain.models.User;

public interface IUserRepository extends IEntityRepository<User> {
    User getUserByID(long id);

    User findUserByLogin(SignInData data);

    User getUserByUsername(String username);

    void addNewUser(SignUpData data);

    void addToBasket(long product_id,String username, int quantity);
}

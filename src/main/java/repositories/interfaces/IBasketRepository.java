package repositories.interfaces;

import domain.models.BasketItem;

import java.util.List;

public interface IBasketRepository {
    List<BasketItem> getBasketItemsByUsername(String username);
    void buyByBasketItemId(long basketItemId, String username);
}

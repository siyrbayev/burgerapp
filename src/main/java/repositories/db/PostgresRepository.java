package repositories.db;

import repositories.interfaces.IDBRepository;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.DriverManager;

public class PostgresRepository implements IDBRepository {
    @Override
    public Connection getConnection() {
        try {
            String connStr = "jdbc:postgresql://localhost:5432/myapp";
            return DriverManager.getConnection(connStr, "postgres", "123");
        } catch (Exception exception) {
            throw new ServerErrorException("Cannot connect to DB: " + exception.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }
}
